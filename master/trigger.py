import requests

from buildbot.plugins import steps, util

from buildbot.schedulers.forcesched import ForceScheduler, ValidationError
from twisted.internet import defer, threads


# We only subclass this so we can rewrite the virtual_builder_name earlier
class ArchForceScheduler(ForceScheduler):
    @defer.inlineCallbacks
    def gatherPropertiesAndChanges(self, collector, **kwargs):
        properties, changeids, sourcestamps = \
            yield super(ArchForceScheduler, self).gatherPropertiesAndChanges(
                collector, **kwargs
            )

        url = yield properties.getProperty("url")
        if not url.endswith("/json"):
            url = properties.getProperty("url")+"/json"
        r = requests.get(url)
        j = r.json()
        yield properties.setProperty("virtual_builder_name", f"Todo: {j['name']}", "", runtime=True)
        yield properties.setProperty("virtual_builder_tags", ["rebuild"], "", runtime=True)
        defer.returnValue((properties, changeids, sourcestamps))


class ArchTrigger(steps.Trigger):
    def __init__(self, *args, **kwargs):
        super(ArchTrigger, self).__init__(*args, **kwargs)

    def setBuildProps(self):
        props = self.set_properties.copy()
        props["reason"] = self.build.getProperty("description", "")
        props["architecture"] = self.build.getProperty("architecture")
        props["build_repository"] = self.build.getProperty("build_repository")
        props["owner"] = self.build.getProperty("owner", "")
        props["should_reproduce"] = self.build.getProperty("should_reproduce", False)
        props["bump_pkgrel"] = self.build.getProperty("bump_pkgrel", False)

        to_repo = self.build.getProperty("to_repository", "")
        branch = self.build.getProperty("branch", "")
        props["branch"] = branch
        checkout = self.build.getProperty("checkout", "")
        props["checkout"] = checkout
        # Essentially; if we don't have to_repository set from the force scheduler,
        # then we can see if we are doing an svn builds and use the branch to set `to_repository`
        # TODO: We need something better structured
        if not to_repo and (branch and checkout == "svn"):
            to_repo = branch.split("-")[0]
        props["to_repository"] = to_repo
        if to_repo:
            props["make_repository"] = True

        for k,v in self.build.getProperty("setconf", {}).items():
            props[k] = v

        return props


class PackageBuildTrigger(ArchTrigger):
    def __init__(self, *args, **kwargs):
        super(PackageBuildTrigger, self).__init__(*args, **kwargs)

    def getSchedulersAndProperties(self):
        sp = []
        for scheduler in self.schedulerNames:
            props = self.setBuildProps()
            package = self.build.getProperty('package', "")
            arch = self.build.getProperty('architecture')
            if not package:
                package = self.build.getProperty('project', "")
            props["pkgbase"] = package
            props["virtual_builder_name"] = f"{package} {arch}"
            props["virtual_builder_tags"] = [arch, "package"]
            sp.append([scheduler, props])
        return sp


class TodolistBuildTrigger(ArchTrigger):
    def __init__(self, *args, **kwargs):
        super(TodolistBuildTrigger, self).__init__(*args, **kwargs)

    def getSchedulersAndProperties(self):
        sp = []
        cache = []
        for scheduler in self.schedulerNames:
            for pkg in self.build.getProperty('packages'):
                if pkg in cache:
                    continue
                props = self.setBuildProps()
                arch = self.build.getProperty('architecture')
                props["virtual_builder_name"] = f"{pkg['pkgbase']} {arch}"
                props["pkgbase"] = pkg['pkgbase']
                sp.append([scheduler, props])
                cache.append(pkg['pkgbase'])
        return sp
