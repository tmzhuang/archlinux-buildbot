from buildbot.plugins import steps, util
from buildbot.process.buildstep import SUCCESS


from buildsteps import *
from trigger import *



def fetch_packages(prop, stdout, stderr):
    pkgs = []
    for pkg in stdout.split("\n"):
        if not pkg:
            continue
        pkgs.append(pkg)
    return {"output_packages": pkgs}


@util.renderer
def get_packages(props):
    ret = []
    if pkgs := props.getProperty("output_packages", []):
        ret.extend(pkgs)
    if patch := props.getProperty("patch_filename", ""):
        ret.append(patch)
    return ret


@util.renderer
def output_directory(props):
    return get_output_dir(props)+"/"


class PackageBuildFactory(util.BuildFactory):
    def __init__(self, ftpdir):
        super().__init__()

        # Prepare our ENV
        workdir = util.Interpolate('build/%(prop:pkgbase)s/trunk')
        self.addSteps([
            steps.SetProperty(property="workdir",
                value=util.Interpolate('build/%(prop:pkgbase)s/trunk')),
            steps.SetProperty(property="ftpdir",
                value=ftpdir),
            Cleanup()
            ])

        # Source checkout
        self.addSteps([
            AspCheckout(),
            AspUpdate(),
            SVNCheckout(),
        ])

        # Set PKGBUILD values
        # TODO:
        #       * pkgver
        #       * _commit
        #       * _tag
        self.addSteps([
            BumpPkgrel(workdir=workdir),
            Setpkgver(workdir=workdir),
            Setpkgrel(workdir=workdir),
            Setcommit(workdir=workdir),
            Settag(workdir=workdir),
            Updpkgsums(workdir=workdir),
            ])

        # Collect future artifacts
        self.addStep(steps.SetPropertyFromCommand(
                name="collect package file names",
                command="makepkg --packagelist",
                workdir=workdir, env={"CARCH": util.Interpolate("%(prop:architecture)s"), "PKGDEST": ""},
                extract_fn=fetch_packages))

        # Package build step
        self.addStep(ArchBuild(workdir=workdir, env={"PKGDEST": ""}))
        self.addStep(ReproducePackage(workdir=workdir, env={"PKGDEST": ""}))
        # self.addStep(ListPackage())

        # Fetch our downstream patch for the package
        self.addSteps([
            GetRepositoryDiff(workdir=workdir),
            steps.SetProperty(property="patch_filename",
                doStepIf=(lambda step: step.getProperty("checkout", "") == ""),
                value=util.Interpolate('%(prop:pkgbase)s/trunk/%(prop:pkgbase)s.patch')),
            ])
       
        # Download artifacts
        self.addStep(steps.MultipleFileUpload(
            workersrcs=get_packages, 
            masterdest=output_directory))
        
        # Maybe make a repository
        self.addStep(RepoAdd())

        self.addSteps([
            Cleanup(),
            ChmodRepo(),
            ])

        self.addStep(DisplayArtifactURL(name="Link to build artifacts"))


class RebuildFactory(util.BuildFactory):
    def __init__(self):
        super().__init__()
        self.addStep(SetPropertyFromTodoList(name="Set todolist information", url=util.Property('url')))
        self.addStep(DisplayDestRepo())
        self.addStep(TodolistBuildTrigger(
            name='Package build list',
            waitForFinish=True,
            schedulerNames=["Trigger Package Build"]))


class BuildFactory(util.BuildFactory):
    def __init__(self):
        super().__init__()
        self.addStep(PackageBuildTrigger(
            name='packagebuild',
            schedulerNames=["Trigger Package Build"],
            ))
        # self.addStep(PackageBuildTrigger(name='packagebuild', schedulerNames=["test-scheduler"]))


class TestFactory(util.BuildFactory):
    def __init__(self):
        super().__init__()
        self.addStep(SeeProperties())
        self.addStep(steps.ShellCommand(command=["echo", "Hello World"]))
