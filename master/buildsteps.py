import os

import requests

from twisted.internet import defer

from buildbot.plugins import steps, util
from buildbot.util.identifiers import forceIdentifier
from buildbot.process.buildstep import SUCCESS
from buildbot.process.properties import Interpolate


def get_output_dir(props):
    ftpdir = props.getProperty("ftpdir")
    owner = props.getProperty("owner").split("@")[0]
    if owner:
        ftpdir += f"/~{owner}"
    repo = props.getProperty("to_repository")
    arch = props.getProperty("architecture")
    if props.getProperty("make_repository", False):
        ftpdir += f"/{repo}/{arch}"
        return ftpdir
    ftpdir += "/builds"
    return ftpdir


class SVNCheckout(steps.SVN):
    name = "checking out package from svn"
    warnOnFailure = 1
    description = ["checkout package from svn"]
    descriptionDone = ["checked out package from svn"]
    workdir=util.Interpolate('build/%(src::project)s/trunk')
    repourl=util.Interpolate('%(src::repository)s/%(src::project)s/trunk')

    def __init__(self, **kwargs):
        super().__init__(repourl=self.repourl, doStepIf=self.doStepIf, workdir=self.workdir, alwaysUseLatest=True, **kwargs)

    @staticmethod
    def doStepIf(step):
        return step.getProperty("checkout", "") == "svn"


class AspCheckout(steps.ShellCommand):
    name = "asp checkout package"
    warnOnFailure = 1
    description = ["checkout package from asp"]
    descriptionDone = ["checked out package"]
    command = ["asp", "checkout", Interpolate("%(prop:pkgbase)s")]

    @staticmethod
    def doStepIf(step):
        return step.getProperty("checkout", "") == ""


class AspUpdate(steps.ShellCommand):
    name = "asp update"
    description = ["updating checkout"]
    descriptionDone = ["updated checkout"]
    command = ["asp", "update", Interpolate("%(prop:pkgbase)s")]

    @staticmethod
    def doStepIf(step):
        return step.getProperty("checkout", "") == ""


class ArchBuild(steps.ShellCommand):
    name = "build package"
    haltOnFailure = 1
    flunkOnFailure = 1
    description = ["building package"]
    descriptionDone = ["built package"]

    @staticmethod
    @util.renderer
    def command(props):
        repo = props.getProperty("build_repository")
        if not repo:
            repo = "extra"
        arch = props.getProperty("architecture")
        return f"{repo}-{arch}-build"


class Cleanup(steps.ShellCommand):
    name = "cleanup"
    haltOnFailure = 1
    flunkOnFailure = 1
    #hideStepIf = True
    description = ["cleaning up"]
    descriptionDone = ["cleaned"]
    command = ["rm", "-rf", Interpolate("%(prop:pkgbase)s")]


class ChmodRepo(steps.MasterShellCommand):
    name = "chmod fix"
    haltOnFailure = 1
    flunkOnFailure = 1
    #hideStepIf = True
    description = ["chmod fix"]
    descriptionDone = ["chmodded"]
    command = ["chmod", "-R", "755", Interpolate("%(prop:ftpdir)s")]

    def __init__(self, **kwargs):
        super().__init__(command=self.command, **kwargs)


class DisplayDestRepo(steps.BuildStep):
    name = "Links for rebuild list"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def run(self):
        ret = []
        props = self.build.getProperties()
        url = get_output_dir(props)
        self.addURL("Link to todolist", props.getProperty('url', ""))
        self.addURL(url, url)
        return defer.succeed(SUCCESS)


class DisplayUrl(steps.BuildStep):
    def __init__(self, text, url, **kwargs):
        super().__init__(**kwargs)
        self.text = text
        self.url = url

    def run(self):
        self.addURL(self.text, self.url)
        return defer.succeed(SUCCESS)


class DisplayArtifactURL(steps.BuildStep):
    name = "Links to build artifacts"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def run(self):
        ret = []
        props = self.build.getProperties()
        owner = props.getProperty("owner")
        if pkgs := props.getProperty("output_packages", []):
            ret.extend(pkgs)
        if patch := props.getProperty("patch_filename", ""):
            ret.append(patch)

        url = get_output_dir(props)
        for i in ret:
            filename = os.path.basename(i)
            furl = url + "/" + filename
            self.addURL(filename, furl)

        return defer.succeed(SUCCESS)


class SetPropertyFromTodoList(steps.BuildStep):
    def __init__(self, url, **kwargs):
        super().__init__(**kwargs)
        self.url = url

    def run(self):
        properties = self.build.getProperties()
        url = properties.getProperty("url")
        if not url.endswith("/json"):
            url = properties.getProperty("url")+"/json"
        r = requests.get(url)
        j = r.json()

        packages_to_build = []
        for pkg in j['packages']:
            if pkg["status_str"] == "Complete":
                continue
            packages_to_build.append(pkg)

        description = f"{j['kind']}: {j['name']}"
        properties.setProperty("packages", packages_to_build, self.name, runtime=True)
        properties.setProperty("description", description, self.name, runtime=True)
        properties.setProperty("to_repository", forceIdentifier(20, j['name']), self.name, runtime=True)
        properties.setProperty("make_repository", True, self.name, runtime=True)
        if properties.getProperty("rebuild_name") == "":
            properties.setProperty("rebuild_name", j['name'], self.name, runtime=True)
        return defer.succeed(SUCCESS)


class RepoAdd(steps.MasterShellCommand):
    haltOnFailure = 1
    flunkOnFailure = 1
    name = "repo-add"
    description = ["adding package to repo"]
    descriptionDone = ["package added to repo"]

    def __init__(self, **kwargs):
        super().__init__(command=self.command, **kwargs)

    @staticmethod
    def doStepIf(step):
        return step.getProperty("make_repository", False)

    # @staticmethod
    # def hideStepIf(results, step):
    #     return not step.getProperty("make_repository", False)

    @staticmethod
    @util.renderer
    def command(props):
        path = get_output_dir(props)
        repo = props.getProperty("to_repository")
        return f"repo-add -R {path}/{repo}.db.tar.gz {path}/*.pkg.tar.*"


class CheckPkg(steps.ShellCommand):
    name = "checkpkg"
    warnOnWarnings = 1
    flunkOnFailure = 0
    haltOnFailure = 0
    warnOnFailure = 0
    description = ["running checkpkg"]
    descriptionDone = ["ran checkpkg"]
    command = ["checkpkg"]


class ListPackage(steps.ShellCommand):
    name = "list package content"
    haltOnFailure = 1
    flunkOnFailure = 1
    description = ["listing package content"]
    descriptionDone = ["listed package content"]
    command = ["tar", "-tf", Interpolate("%(prop:package)s")]


class ReproducePackage(steps.ShellCommand):
    name = "makerepropkg"
    warnOnFailure = 1
    description = ["checking if package is reproducible"]
    descriptionDone = ["checked package reproducability"]
    command = ["makerepropkg"]

    @staticmethod
    def doStepIf(step):
        return step.getProperty("should_reproduce", False)

    # @staticmethod
    # def hideStepIf(results, step):
    #     return not step.getProperty("should_reproduce", False)


class GetRepositoryDiff(steps.ShellCommand):
    name = "diff"
    haltOnFailure = 1
    flunkOnFailure = 1
    description = ["diffing repo"]
    descriptionDone = ["diffed repo"]

    @staticmethod
    @util.renderer
    def command(props):
        pkg = props.getProperty("pkgbase")
        return f"git diff > {pkg}.patch"

    @staticmethod
    def doStepIf(step):
        return step.getProperty("checkout", "") == ""


class BumpPkgrel(steps.ShellCommand):
    name = "bump pkgrel"
    haltOnFailure = 1
    flunkOnFailure = 1
    description = ["bumping pkgrel"]
    descriptionDone = ["bumped"]

    @staticmethod
    @util.renderer
    def command(props):
        return f"setconf PKGBUILD pkgrel+=1"

    @staticmethod
    def doStepIf(step):
        return step.getProperty("bump_pkgrel", False)

    @staticmethod
    def hideStepIf(results, step):
        return not step.getProperty("bump_pkgrel", False)


class Setpkgver(steps.ShellCommand):
    name = "set pkgver"
    haltOnFailure = 1
    flunkOnFailure = 1
    description = ["setting pkgver"]
    descriptionDone = ["sat pkgver"]

    @staticmethod
    @util.renderer
    def command(props):
        props.setProperty("updpkgsums", True, "Setpkgver", runtime=True)
        if pkgver := props.getProperty("set_pkgver", ""):
            return f"setconf PKGBUILD pkgver {pkgver}"
        return ""

    @staticmethod
    def doStepIf(step):
        return step.getProperty("set_pkgver", ""),

    @staticmethod
    def hideStepIf(results, step):
        return not step.getProperty("set_pkgver", ""),


class Setpkgrel(steps.ShellCommand):
    name = "set pkgrel"
    haltOnFailure = 1
    flunkOnFailure = 1
    description = ["setting pkgrel"]
    descriptionDone = ["sat pkgrel"]

    @staticmethod
    @util.renderer
    def command(props):
        if pkgrel := props.getProperty("set_pkgrel", ""):
            return f"setconf PKGBUILD pkgrel {pkgrel}"
        return ""

    @staticmethod
    def doStepIf(step):
        return step.getProperty("set_pkgrel", ""),
        

    @staticmethod
    def hideStepIf(results, step):
        return not step.getProperty("set_pkgrel", ""),


class Setcommit(steps.ShellCommand):
    name = "set _commit"
    haltOnFailure = 1
    flunkOnFailure = 1
    description = ["setting _commit"]
    descriptionDone = ["sat _commit"]

    @staticmethod
    @util.renderer
    def command(props):
        if commit := props.getProperty("set_commit", ""):
            return f"setconf PKGBUILD _commit {commit}"
        return ""

    @staticmethod
    def doStepIf(step):
        return step.getProperty("set_commit", ""),

    @staticmethod
    def hideStepIf(results, step):
        return not step.getProperty("set_commit", ""),


class Settag(steps.ShellCommand):
    name = "set _tag"
    haltOnFailure = 1
    flunkOnFailure = 1
    description = ["setting _tag"]
    descriptionDone = ["sat _tag"]

    @staticmethod
    @util.renderer
    def command(props):
        if tag := props.getProperty("set_tag", ""):
            return f"setconf PKGBUILD _tag {tag}"
        return ""

    @staticmethod
    def doStepIf(step):
        return step.getProperty("set_tag", ""),

    @staticmethod
    def hideStepIf(results, step):
        return not step.getProperty("set_tag", ""),


class Updpkgsums(steps.ShellCommand):
    name = "updated checksums"
    haltOnFailure = 1
    flunkOnFailure = 1
    description = ["setting checksums"]
    descriptionDone = ["setting checksums"]

    @staticmethod
    @util.renderer
    def command(props):
        return "updpkgsums"

    @staticmethod
    def doStepIf(step):
        return step.getProperty("updpkgsums", False)

    @staticmethod
    def hideStepIf(results, step):
        return not step.getProperty("updpkgsums", False)


class SeeProperties(steps.BuildStep):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def run(self):
        properties = self.build.getProperties()
        print(properties)
        return defer.succeed(SUCCESS)
