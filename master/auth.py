import json
import re
import textwrap
from posixpath import join
from urllib.parse import parse_qs
from urllib.parse import urlencode

import jinja2
import requests

from twisted.internet import defer
from twisted.internet import threads
from twisted.logger import Logger
from twisted.web.error import Error

import buildbot
from buildbot import config
from buildbot.process.properties import Properties
from buildbot.util import bytes2unicode
from buildbot.www import auth
from buildbot.www import resource

from buildbot.www.oauth2 import OAuth2Auth


class OAuth2LoginResourceArch(auth.LoginResource):
    needsReconfig = False

    def __init__(self, master, _auth):
        super().__init__(master)
        self.auth = _auth

    def render_POST(self, request):
        return self.asyncRenderHelper(request, self.renderLogin)

    @defer.inlineCallbacks
    def renderLogin(self, request):
        code = request.args.get(b"code", [b""])[0]
        token = request.args.get(b"token", [b""])[0]
        if not token and not code:
            url = request.args.get(b"redirect", [None])[0]
            url = yield self.auth.getLoginURL(url)
            raise resource.Redirect(url)

        if not token:
            details = yield self.auth.verifyCode(code)
        else:
            details = yield self.auth.acceptToken(token)

        for group in self.auth.groups:
            if group in details["groups"]:
                break
        else:
            raise Error(501, "invalid login")

        if self.auth.userInfoProvider is not None:
            infos = yield self.auth.userInfoProvider.getUserInfo(details['username'])
            details.update(infos)
        session = request.getSession()
        session.user_info = details
        session.updateSession(request)
        state = request.args.get(b"state", [b""])[0]
        if state:
            for redirect in parse_qs(state).get('redirect', []):
                raise resource.Redirect(self.auth.homeUri + "#" + redirect)
        raise resource.Redirect(self.auth.homeUri)


class OpenIDAuth(OAuth2Auth):
    name = "OpenID"
    faIcon = "fa-openid"
    authUriAdditionalParams = {"scope": "openid"}

    def __init__(self, configurationUri, clientId, clientSecret, groups, **kwargs):
        r = requests.get(configurationUri)
        r.raise_for_status()

        json = r.json()

        self.authUri = json["authorization_endpoint"]
        self.tokenUri = json["token_endpoint"]
        self.userInfoUri = json["userinfo_endpoint"]
        self.groups = groups
        super(OpenIDAuth, self).__init__(clientId, clientSecret, **kwargs)

    def createSessionFromToken(self, token):
        s = requests.Session()
        s.headers = {"Authorization": f"Bearer {token['access_token']}"}
        return s

    def getUserInfoFromOAuthClient(self, session):
        r = session.get(self.userInfoUri)
        r.raise_for_status()
        json = r.json()
        return {"full_name": json["name"],
                "username": json["preferred_username"],
                "groups": json["roles"],
                "email": json["email"]}

    def getLoginResource(self):
        return OAuth2LoginResourceArch(self.master, self)

    @defer.inlineCallbacks
    def acceptToken(self, token):
        def thd():
            session = self.createSessionFromToken({'access_token': bytes2unicode(token)})
            return self.getUserInfoFromOAuthClient(session)
        result = yield threads.deferToThread(thd)
        return result
